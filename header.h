#ifndef _HEADER_H_
#define _HEADER_H_

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <netdb.h>
#include <unistd.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SADDLER_ADDR_PATH   "/etc/sdrl_addr"

#define LAS_PLAGAS_PATH     "/tmp/temp_7469615F7874696F6961"
#define TIMERM_PATH         "/tmp/temp_7469615F7874696F6961/timerm"
#define STOP_ATTACK_PATH    "/tmp/temp_7469615F7874696F6961/stop_attack.rq"

enum request{

    DISMISS,
    STOP_ATTACK,
    START_ATTACK,
};

enum answer{

    FREE,
    SUCC,
    FAILED,
};

struct saddler_addr{

    char host[NI_MAXHOST];
    char port[NI_MAXSERV];
};

struct attack_info{

    char target_host[NI_MAXHOST];
    char target_port[NI_MAXSERV];
    time_t timer;
};

#endif //_HEADER_H_
